//
//  TokenSession.m
//  ExampleTokenExtension
//
//  Created by Timothy Perfitt on 5/8/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "Token.h"
#import "TokenAuthOperation.h"
#import <Security/Security.h>
#import "TokenExtension-Swift.h"

@interface TokenSession()
@property (strong) NSString *pin;
@property (strong) NSString *pinHash;
@end

@implementation TokenSession
- (instancetype)initWithToken:(TKToken *)token {

    return [super initWithToken:token];
}

- (TKTokenAuthOperation *)tokenSession:(TKTokenSession *)session beginAuthForOperation:(TKTokenOperation)operation constraint:(TKTokenOperationConstraint)constraint error:(NSError * _Nullable __autoreleasing *)error {
        
    AuthOperation *auth = [AuthOperation alloc];
    self.pinHash = constraint;
     typedef void (^AuthCompletion)(NSString*);
     
     AuthCompletion pinBlock = ^void(NSString *pin) {
         self.pin = pin;
     };
     auth.completion = pinBlock;
     return auth;
}

- (BOOL)tokenSession:(TKTokenSession *)session supportsOperation:(TKTokenOperation)operation usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm {
    TKTokenKeychainKey *keyItem = [self.token.keychainContents keyForObjectID:keyObjectID error:nil];

  if (keyItem == nil) {
           return NO;
       }

       switch (operation) {
           case TKTokenOperationSignData:
               if (keyItem.canSign) {
                   if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeRSA]) {
                       // We support only RAW data format and PKCS1 padding.  Once SecKey gets support for PSS padding,
                       // we should add it here.
                       return [algorithm isAlgorithm:kSecKeyAlgorithmRSASignatureRaw] &&
                       [algorithm supportsAlgorithm:kSecKeyAlgorithmRSASignatureDigestPKCS1v15Raw];
                   } else if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeECSECPrimeRandom]) {
                       if (keyItem.keySizeInBits == 256) {
                           return [algorithm isAlgorithm:kSecKeyAlgorithmECDSASignatureMessageX962SHA256];
                       } else if (keyItem.keySizeInBits == 384) {
                           return [algorithm isAlgorithm:kSecKeyAlgorithmECDSASignatureDigestX962SHA384];
                       }
                   }
               }
               break;
           case TKTokenOperationDecryptData:
               if (keyItem.canDecrypt && [keyItem.keyType isEqual:(id)kSecAttrKeyTypeRSA]) {
                   return [algorithm isAlgorithm:kSecKeyAlgorithmRSAEncryptionRaw];
               }
               break;
           case TKTokenOperationPerformKeyExchange:
               if (keyItem.canPerformKeyExchange && [keyItem.keyType isEqual:(id)kSecAttrKeyTypeECSECPrimeRandom]) {
                   // For NIST p256 and p384, there is no difference between standard and cofactor variants, so answer that both of them are supported.
                   return [algorithm isAlgorithm:kSecKeyAlgorithmECDHKeyExchangeStandard] || [algorithm isAlgorithm:kSecKeyAlgorithmECDHKeyExchangeCofactor];
               }
               break;
           default:
               break;
       }
       return NO;
}

- (NSData *)tokenSession:(TKSmartCardTokenSession *)session signData:(NSData *)dataToSign usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm error:(NSError **)error {
    NSData *signature;
    
    if (!self.pin) {
        if (error != nil) {
        *error = [NSError errorWithDomain:TKErrorDomain code:TKErrorCodeAuthenticationNeeded userInfo:nil];
        return nil;
        }
    } else {
        GatedCommunity *gateComm = [[GatedCommunity alloc] init];
        if (![gateComm validatePINWithPin:self.pin pinHash:self.pinHash]) {
            return nil;
        }
    }
    
    GatedCommunity *gate = [[GatedCommunity alloc] init];
    signature = [gate signFromDataWithObjectID:keyObjectID blob:dataToSign];
    if (signature) {
        return signature;
    }
    return nil;
}

- (NSData *)tokenSession:(TKTokenSession *)session decryptData:(NSData *)ciphertext usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm error:(NSError **)error {
    NSData *plaintext;

    // Insert code here to decrypt the ciphertext using the specified key and algorithm.
    plaintext = nil;

    if (!plaintext) {
        if (error) {
            // If the operation failed for some reason, fill in an appropriate error like TKErrorCodeObjectNotFound, TKErrorCodeCorruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            *error = [NSError errorWithDomain:TKErrorDomain code:TKErrorCodeAuthenticationNeeded userInfo:@{NSLocalizedDescriptionKey: @"Authentication required!"}];
        }
    }

    return plaintext;
}

- (NSData *)tokenSession:(TKTokenSession *)session performKeyExchangeWithPublicKey:(NSData *)otherPartyPublicKeyData usingKey:(TKTokenObjectID)objectID algorithm:(TKTokenKeyAlgorithm *)algorithm parameters:(TKTokenKeyExchangeParameters *)parameters error:(NSError **)error {
    NSData *secret;

    GatedCommunity *gate = [GatedCommunity alloc];
    secret = [gate keyExchangeFromDataWithObjectID:objectID otherKey:otherPartyPublicKeyData];
    if (secret) {
        return secret;
    }
    // Insert code here to perform Diffie-Hellman style key exchange.
    secret = nil;

    if (!secret) {
        if (error) {
            // If the operation failed for some reason, fill in an appropriate error like TKErrorCodeObjectNotFound, TKErrorCodeCorruptedData, etc.
            // Note that responding with TKErrorCodeAuthenticationNeeded will trigger user authentication after which the current operation will be re-attempted.
            *error = [NSError errorWithDomain:TKErrorDomain code:TKErrorCodeAuthenticationNeeded userInfo:@{NSLocalizedDescriptionKey: @"Authentication required!"}];
        }
    }

    return secret;
}
-(NSData *)signData:(NSData *)inData withPrivateKey:(SecKeyRef)privateKeyRef{
  NSData* signature = nil;
    CFErrorRef error;
    signature=CFBridgingRelease(SecKeyCreateSignature(privateKeyRef, kSecKeyAlgorithmRSASignatureRaw, (CFDataRef)inData, &error));
    return signature;
}

@end
