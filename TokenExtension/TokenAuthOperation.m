//
//  TokenAuthOperation.m
//  ExampleTokenExtension
//
//  Created by Timothy Perfitt on 5/9/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "TokenAuthOperation.h"

@implementation TokenAuthOperation
- (instancetype)initWithSession:(TKTokenSession *)session {
    if (self = [super init]) {

    }

    return self;
}

// Remove this as soon as PIVAuthOperation implements automatic PIN submission according to APDUTemplate.
- (BOOL)finishWithError:(NSError * _Nullable __autoreleasing *)error {
    NSMutableData *PINData = [NSMutableData dataWithLength:8];
    memset(PINData.mutableBytes, 0xff, PINData.length);
    [[self.PIN dataUsingEncoding:NSUTF8StringEncoding] getBytes:PINData.mutableBytes length:PINData.length];

    return YES;
}



@end
