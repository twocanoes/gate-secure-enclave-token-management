//
//  PKCS7.m
//  SMIME Reader
//
//  
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//


#import "X509.h"
#import "NSData+HexString.h"
#define ENCRYPTED_CONTENT_INFO @"0.2" 
#define VERSION @"0.0"
#define RECIPIENTINFOS @"0.1"
#define ASN1_CLASS_UNIVERSAL 0
#define ASN1_CLASS_APPLICATION 64
#define ASN1_CLASS_CONTENT_SPECIFIC 128
#define ASN1_CLASS_PRIVATE 192
#define ASN1_CATEGORY_COMPOSITE 32
#define ASN1_CATEGORY_PRIMATIVE 0


#define ASN1_TYPE_INTEGER	2	
#define ASN1_TYPE_BIT_STRING	3	
#define ASN1_TYPE_OCTET_STRING 4
#define ASN1_TYPE_NULL	5
#define ASN1_TYPE_OBJECT_IDENTIFIER	6
#define ASN1_TYPE_UTF8STRING    12
#define ASN1_TYPE_SEQUENCE	16
#define ASN1_TYPE_SET	17
#define ASN1_TYPE_PRINTABLESTRING	19	
#define ASN1_TYPE_T61STRING	20
#define ASN1_TYPE_IA5STRING	22
#define ASN1_TYPE_UTCTIME	23
#define OID_ENVELOPED_DATA @"1.2.840.113549.1.7.3"
#define OID_SIGNED_DATA @"1.2.840.113549.1.7.2"
//#define OID_RSA @"1.2.840.113549.1.1.1"
#define OID_ECDSA_WITH_SHA256 @"1.2.840.10045.4.3.2"
#define OID_RSA_DATA @"1.2.840.113549.1.7.1"
#define OID_3DES @"1.2.840.113549.3.7"
#define OID_SHA1 @"1.2.840.113549.1.1.5"
#define OID_MESSAGE_DIGEST @"1.2.840.113549.1.9.4"
#define OID_ALG_SHA1 @"1.3.14.3.2.26"
#define OID_CONTENT_TYPE @"1.2.840.113549.1.9.3"
#define OID_RSA_ENCRYPTION @"1.2.840.113549.3"
#define OID_COMMON_NAME @"2.5.4.3"
#define OID_EC_PUBLIC_KEY @"1.2.840.10045.2.1"
#define OID_X592_PRIME256V1 @"1.2.840.10045.3.1.7"
@implementation X509
@synthesize pkcs7Array,content,contentType;

+(ASN1Composite *)signedCertificateWithCertificateData:(NSData *)certData signatureData:(NSData *)signatureData{

    ASN1Composite *certificate=[ASN1Composite asn1Composite];
    [certificate setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    ASN1Primative *tbsCertificate=[ASN1Primative asn1PrimativeWithData:certData];
    [certificate addObject:tbsCertificate];

    ASN1Composite *signatureAlgorithmComposite=[ASN1Composite asn1Composite];
    [signatureAlgorithmComposite setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [certificate addObject:signatureAlgorithmComposite];
    ASN1Primative *signatureAlgorithm=[ASN1Primative asn1PrimativeWithOID:OID_ECDSA_WITH_SHA256];
    [signatureAlgorithmComposite addObject:signatureAlgorithm];


    NSMutableData *rawDataWithLeadingZero=[[NSData dataWithHexString:@"00"] mutableCopy];
    [rawDataWithLeadingZero appendData:signatureData];


    ASN1Primative *signatureBitString=[ASN1Primative asn1PrimativeWithBitString:rawDataWithLeadingZero];
    [certificate addObject:signatureBitString];

    return certificate;

}

+(ASN1Composite *)createX509CertificateWithCommonName:(NSString *)cn publicKey:(NSData *)publicKey{

    ASN1Composite *tbsCertificate=[ASN1Composite asn1Composite];
    [tbsCertificate setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    ASN1Composite *versionComposite=[ASN1Composite asn1Composite];
    [versionComposite setClass:ASN1_CLASS_CONTENT_SPECIFIC type:0 category:ASN1_CATEGORY_COMPOSITE];


    ASN1Primative *version=[ASN1Primative asn1PrimativeWithInteger:[NSData dataWithBytes:(unsigned char []){0x02} length:1]];

    [versionComposite addObject:version];
    [tbsCertificate addObject:versionComposite];

    ASN1Primative *certificateSerialNumber=[ASN1Primative asn1PrimativeWithInteger:[NSData dataWithHexString:@"00C4501B7E7F5BB2FF"]];
    [tbsCertificate addObject:certificateSerialNumber];

    ASN1Composite *keyEncryptionAlgorithm=[ASN1Composite asn1Composite];
    [keyEncryptionAlgorithm setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [keyEncryptionAlgorithm addObject:[ASN1Primative asn1PrimativeWithOID:OID_ECDSA_WITH_SHA256]];
    [tbsCertificate addObject:keyEncryptionAlgorithm];

    ASN1Composite *issuerChoice=[ASN1Composite asn1Composite];
    [issuerChoice setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [tbsCertificate addObject:issuerChoice];
    ASN1Composite *relativeDNSet=[ASN1Composite asn1Composite];
    [relativeDNSet setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SET category:ASN1_CATEGORY_COMPOSITE];

    [issuerChoice addObject:relativeDNSet];

    ASN1Composite *dnAttributeTypeAndValueComposite=[ASN1Composite asn1Composite];
    [dnAttributeTypeAndValueComposite setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [relativeDNSet addObject:dnAttributeTypeAndValueComposite];
    ASN1Primative *dnType=[ASN1Primative asn1PrimativeWithOID:OID_COMMON_NAME];
    [dnAttributeTypeAndValueComposite addObject:dnType];

    ASN1Primative *dnValue=[ASN1Primative asn1PrimativeWithUTF8String:cn];
    [dnAttributeTypeAndValueComposite addObject:dnValue];


    ASN1Composite *validDatesComposite=[ASN1Composite asn1Composite];
    [validDatesComposite setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [tbsCertificate addObject:validDatesComposite];

    ASN1Primative *validAfterDate=[ASN1Primative asn1PrimativeWithDate:[NSDate date]];

    [validDatesComposite addObject:validAfterDate];

    ASN1Primative *validBeforeDate=[ASN1Primative asn1PrimativeWithDate:[NSDate dateWithTimeIntervalSinceNow:60*60*24*365]];

    [validDatesComposite addObject:validBeforeDate];



    ASN1Composite *subjectChoice=[ASN1Composite asn1Composite];
    [subjectChoice setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [tbsCertificate addObject:subjectChoice];
    ASN1Composite *relativeSubjectDNSet=[ASN1Composite asn1Composite];
    [relativeSubjectDNSet setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SET category:ASN1_CATEGORY_COMPOSITE];

    [subjectChoice addObject:relativeSubjectDNSet];

    ASN1Composite *subjectDNAttributeTypeAndValueComposite=[ASN1Composite asn1Composite];
    [subjectDNAttributeTypeAndValueComposite setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [relativeSubjectDNSet addObject:subjectDNAttributeTypeAndValueComposite];
    ASN1Primative *subjectDNType=[ASN1Primative asn1PrimativeWithOID:OID_COMMON_NAME];
    [subjectDNAttributeTypeAndValueComposite addObject:subjectDNType];

    ASN1Primative *subjectDNValue=[ASN1Primative asn1PrimativeWithUTF8String:cn];
    [subjectDNAttributeTypeAndValueComposite addObject:subjectDNValue];



    ASN1Composite *subjectPublicKeyInfo=[ASN1Composite asn1Composite];
    [subjectPublicKeyInfo setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [tbsCertificate addObject:subjectPublicKeyInfo];

    ASN1Composite *subjectPublicKeyInfoAlgorithm=[ASN1Composite asn1Composite];
    [subjectPublicKeyInfoAlgorithm setClass:ASN1_CLASS_UNIVERSAL type:ASN1_TYPE_SEQUENCE category:ASN1_CATEGORY_COMPOSITE];

    [subjectPublicKeyInfo addObject:subjectPublicKeyInfoAlgorithm];


    [subjectPublicKeyInfoAlgorithm addObject:[ASN1Primative asn1PrimativeWithOID:OID_EC_PUBLIC_KEY]];
    [subjectPublicKeyInfoAlgorithm addObject:[ASN1Primative asn1PrimativeWithOID:OID_X592_PRIME256V1]];


    NSMutableData *updatedPublicKey=[[NSData dataWithHexString:@"00"] mutableCopy];
    if (publicKey.length==65) {
        [updatedPublicKey appendData:publicKey];

    }
    else if (publicKey.length==66){
        [updatedPublicKey setData:publicKey];
    }
    else {

        NSLog(@"bad public key!. Length is not 65 or 66 bytes");
        return nil;
    }
    if (![[updatedPublicKey subdataWithRange:NSMakeRange(0, 2)] isEqualToData:[NSData dataWithHexString:@"0004"]]){

        NSLog(@"invalid prefix in public key. Make sure it is in the x963 format");
        return nil;
    }

    ASN1Primative *publicKeyBitString=[ASN1Primative asn1PrimativeWithBitString:updatedPublicKey];
    [subjectPublicKeyInfo addObject:publicKeyBitString];

    return tbsCertificate;
}

+(NSData *)encodeASN1:(ASN1Composite *)composite useINF:(BOOL)useINF{

	//static unsigned char infEnd[]={0x00,0x00};
	//NSData *infEndData=[NSData dataWithBytes:infEnd length:2];



	NSMutableData *finalData=[NSMutableData data];
	int type=[composite asn1Type]|[composite asn1Class]|[composite asn1Category];

	NSData *typeData=[NSData dataWithBytes:&type length:1];
	[finalData appendData:typeData];

	NSMutableData *innerData=[NSMutableData data];

	for (id currentASN in composite) {
		if ([currentASN asn1Category]==ASN1_CATEGORY_COMPOSITE) {
			NSData *compositeData=[X509 encodeASN1:currentASN useINF:NO];
			[innerData appendData:compositeData];

		}
		else {
			NSData *newPrimativeData=nil;
			ASN1Primative *primative=(ASN1Primative *)currentASN;
			int primativeType=[currentASN asn1Type]|[currentASN asn1Class]|[currentASN asn1Category];
			NSData *primativeTypeData=[NSData dataWithBytes:&primativeType length:1];



			if (primative.asn1Type==ASN1_TYPE_OCTET_STRING){
				newPrimativeData=[primative primativeData];

			}

			else if (primative.asn1Type==ASN1_TYPE_PRINTABLESTRING){
				NSString *printableString=(NSString *)[primative primativeData];
//				int length=[printableString length];
//				NSData *lengthData=[NSData dataWithBytes:&length length:1];
//				[finalData appendData:lengthData];
				NSData *stringData=[printableString dataUsingEncoding:NSUTF8StringEncoding];
//				[finalData appendData:stringData];
				newPrimativeData=stringData;

			}
            else if (primative.asn1Type==ASN1_TYPE_UTF8STRING){
                NSString *printableString=(NSString *)[primative primativeData];
                NSData *stringData=[printableString dataUsingEncoding:NSUTF8StringEncoding];
                newPrimativeData=stringData;

            }

			else if (primative.asn1Type==ASN1_TYPE_INTEGER){
				newPrimativeData=(NSData *)[primative primativeData];

//				newPrimativeData=[NSData dataWithBytes:&integer length:sizeof(int)];

			}
            else if (primative.asn1Type==ASN1_TYPE_UTCTIME){

                NSDate *primativeDate=(NSDate *)[primative primativeData];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                formatter.timeZone=[NSTimeZone timeZoneWithName:@"GMT"];
                [formatter setDateFormat:@"YYMMddHHmmss'Z'"];

                NSString *dateString=[formatter stringFromDate:primativeDate];
                NSData *stringData=[dateString dataUsingEncoding:NSUTF8StringEncoding];
                newPrimativeData=stringData;

            }

			else if (primative.asn1Type==ASN1_TYPE_OBJECT_IDENTIFIER){

				NSMutableData *outData;
				NSData *currentOctetData=nil;
				NSMutableData *newData;
				NSString *inOID=(NSString *)[primative primativeData];

				NSArray *oidArray=[inOID componentsSeparatedByString:@"."];

				int firstByte=[[oidArray objectAtIndex:0] intValue]*40+[[oidArray objectAtIndex:1] intValue];

//				NSLog(@" byte is %x",firstByte);

				outData=[NSMutableData dataWithBytes:&firstByte length:1];
				int i;
				for (i=2;i<[oidArray count];i++) {

					int workingInt=[[oidArray objectAtIndex:i] intValue];
					int currentOID=workingInt&0x7F;

					newData=[NSMutableData dataWithBytes:&currentOID length:1];
					if (currentOctetData!=nil) [newData appendData:currentOctetData];
					currentOctetData=newData;

					workingInt=workingInt>>7;
					while (workingInt!=0) {
						currentOID=workingInt&0x7F;
						currentOID=currentOID|0X80;

						newData=[NSMutableData dataWithBytes:&currentOID length:1];
						[newData appendData:currentOctetData];
						currentOctetData=newData;
						workingInt=workingInt>>7;
					}
					[outData appendData:currentOctetData];
					currentOctetData=nil;

				}
				newPrimativeData=outData;

			}
			else if (primative.asn1Type==ASN1_RAW_DATA){
				newPrimativeData=[primative primativeData];



			}
            else if (primative.asn1Type==ASN1_TYPE_BIT_STRING){
                newPrimativeData=[primative primativeData];

            }
			if (primative.asn1Type!=ASN1_RAW_DATA) {

				[innerData appendData:primativeTypeData];
				[innerData appendData:[X509 encodedLengthFromData:newPrimativeData]];

//				int length=[newPrimativeData length];
//
//				if (length<0X80) {
//					lengthData=[NSData dataWithBytes:&length length:1];
//					[innerData appendData:lengthData];
//				}
//				else {
//					length=CFSwapInt32HostToBig(length);
//					lengthData=[NSData dataWithBytes:&length length:sizeof(int)];
//					int counter=0;
//					NSData *currData=[lengthData subdataWithRange:NSMakeRange(counter, 1)];
//					int currInt=0;
//					[currData getBytes:&currInt length:1];
//					while (currInt==0) {
//						counter++;
//						currData=[lengthData subdataWithRange:NSMakeRange(counter, 1)];
//						currInt=0;
//						[currData getBytes:&currInt length:1];
//					}
//					NSData *strippedData=[lengthData subdataWithRange:NSMakeRange(counter, [lengthData length]-counter)];
//					int strippedLength=[strippedData length];
//					strippedLength=strippedLength|0x80;
//					NSData *strippedLengthData=[NSData dataWithBytes:&strippedLength length:1];
//					[innerData appendData:strippedLengthData];
//					[innerData appendData:strippedData];


				//}

			}

			[innerData appendData:newPrimativeData];

		}

	}
	/*
	 static unsigned char infEnd[]={0x00,0x00};
	 NSData *infEndData=[NSData dataWithBytes:infEnd length:2];



	 NSMutableData *finalData=[NSMutableData data];
	 int type=[composite asn1Type]|[composite asn1Class]|[composite asn1Category];
	 NSData *typeData=[NSData dataWithBytes:&type length:1];
	 [finalData appendData:typeData];

	 int length=0x80; //INF
	 NSData *lengthData=[NSData dataWithBytes:&length length:1];
	 [finalData appendData:lengthData];


	 */
	int length=(int)[innerData length];
	if (useINF==YES || length>1000) {

		int length=0x80; //INF
		NSData *lengthData=[NSData dataWithBytes:&length length:1];

		static unsigned char infEnd[]={0x00,0x00};
		NSData *infEndData=[NSData dataWithBytes:infEnd length:2];


		[finalData appendData:lengthData];
		[finalData appendData:innerData];
		[finalData appendData:infEndData];
	}
	else{
		[finalData appendData:[X509 encodedLengthFromData:innerData]];
		[finalData appendData:innerData];
	}


	return finalData;
}

+(NSData *)encodedLengthFromData:(NSData *)inData{

	if ([inData length]>0XFFFFFFF) NSLog(@"length too long!");
	int length=(int)[inData length];
	if (length<0x7f) {
		NSData *lengthData=[NSData dataWithBytes:&length length:1];
		return lengthData;
	}
	else {

		int bigLength=CFSwapInt32HostToBig(length);

		NSData *lengthByteData=[NSData dataWithBytes:&bigLength length:sizeof(bigLength)];

//		NSMutableData *shortendLengthByteData=[NSMutableData data];
		int currByte;
		int pos=0;
		while (pos<[lengthByteData length]) {
			currByte=0;
			NSData *currByteData=[lengthByteData subdataWithRange:NSMakeRange(pos, 1)];
			[currByteData getBytes:&currByte length:1];
			if (currByte!=0x00) break;
			pos++;
			//[shortendLengthByteData appendData:currByteData];

		}
		if (pos==[lengthByteData length]) {
			NSLog(@"zero byte length!!");
			return nil;
		}
		NSData *shortendLengthByteData=[lengthByteData subdataWithRange:
										NSMakeRange(pos, [lengthByteData length]-pos)];
		int lengthOfLength=(int)[shortendLengthByteData length];
		//just realized that only using int limits to 0xFFFFFFFF bytes length of data!
		lengthOfLength=0X80|lengthOfLength;

		NSData *lengthOfLengthData=[NSData dataWithBytes:&lengthOfLength length:1];

		NSMutableData *returnData=[NSMutableData dataWithData:lengthOfLengthData];
		[returnData appendData:shortendLengthByteData];

		return returnData;

	}

}

@end
