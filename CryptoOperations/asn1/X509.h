//
//  PKCS7.h
//  SMIME Reader
//
//  
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import "DER.h"
#import "ASN1Primative.h"
#import "NSData+Base64.h"
@interface X509 : NSObject {

	ASN1Composite *certificateArray;
	NSString *contentType;
	ASN1Composite *content;

}
@property(retain) ASN1Composite *pkcs7Array;
@property(retain) NSString *contentType;
@property(retain) ASN1Composite *content;

+(ASN1Composite *)createX509CertificateWithCommonName:(NSString *)cn publicKey:(NSData *)publicKey;
+(NSData *)encodeASN1:(ASN1Composite *)composite useINF:(BOOL)useINF;
+(ASN1Composite *)signedCertificateWithCertificateData:(NSData *)certData signatureData:(NSData *)signatureData;
@end
