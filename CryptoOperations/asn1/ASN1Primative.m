//
//  ASN1Primative.m
//  SMIME Reader
//
//  
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "ASN1Primative.h"

/*
 #define ASN1_TYPE_INTEGER	2	
 #define ASN1_TYPE_BIT_STRING	3	
 #define ASN1_TYPE_OCTET_STRING 4
 #define ASN1_TYPE_NULL	5
 #define ASN1_TYPE_OBJECT_IDENTIFIER	6
 #define ASN1_TYPE_SEQUENCE	16
 #define ASN1_TYPE_SET	17
 #define ASN1_TYPE_PRINTABLESTRING	19	
 #define ASN1_TYPE_T61STRING	20
 #define ASN1_TYPE_IA5STRING	22
 #define ASN1_TYPE_UTCTIME	23
#define 
 */
@implementation ASN1Primative
@synthesize asn1Type,asn1Category,asn1Class,primativeData;

+(ASN1Primative *)asn1PrimativeWithOID:(NSString *)oid{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:oid];
	primative.asn1Type=ASN1_TYPE_OBJECT_IDENTIFIER;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
+(ASN1Primative *)asn1PrimativeWithInteger:(NSData *)inIntData{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inIntData];
	primative.asn1Type=ASN1_TYPE_INTEGER;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
+(ASN1Primative *)asn1PrimativeWithBitString:(NSData *)inBitString{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inBitString];
	primative.asn1Type=ASN1_TYPE_BIT_STRING;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
+(ASN1Primative *)asn1PrimativeWithPrintableString:(NSString *)inString{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inString];
	primative.asn1Type=ASN1_TYPE_PRINTABLESTRING;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
+(ASN1Primative *)asn1PrimativeWithUTF8String:(NSString *)inString{
    ASN1Primative *primative=[[ASN1Primative alloc] init];
    [primative setPrimativeData:inString];
    primative.asn1Type=ASN1_TYPE_UTF8STRING;
    primative.asn1Class=ASN1_CLASS_UNIVERSAL;
    primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
    return primative;

}
+(ASN1Primative *)asn1PrimativeWithData:(NSData *)inData{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inData];
	primative.asn1Type=ASN1_RAW_DATA;
	primative.asn1Class=ASN1_RAW_DATA;
	primative.asn1Category=ASN1_RAW_DATA;
	return primative;
	
}


+(ASN1Primative *)asn1PrimativeWithOctetString:(NSData *)inData{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inData];
	primative.asn1Type=ASN1_TYPE_OCTET_STRING;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}

+(ASN1Primative *)asn1PrimativeWithDate:(NSDate *)inDate{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inDate];
	primative.asn1Type=ASN1_TYPE_UTCTIME;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
+(ASN1Primative *)asn1PrimativeWithID:(id)inVal{
	ASN1Primative *primative=[[ASN1Primative alloc] init];
	[primative setPrimativeData:inVal];
	primative.asn1Type=ASN1_TYPE_OCTET_STRING;
	primative.asn1Class=ASN1_CLASS_UNIVERSAL;
	primative.asn1Category=ASN1_CATEGORY_PRIMATIVE;
	return primative;
	
}
- (NSString *)descriptionWithLocale:(id)locale{
		return [self descriptionWithLocale:locale indent:0];
}
- (NSString *)description{
	
	return [self descriptionWithLocale:nil indent:0];
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level{
	return [NSString stringWithFormat:@"Type:%i,Category:%i,Class:%i,Data:%@",
			asn1Type,asn1Category,asn1Class,primativeData];
	
	
}


@end
