//
//  CryptoManager.m
//  CryptoOperations
//
//  Created by Timothy Perfitt on 5/11/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "CryptoManager.h"
#import "X509.h"
#import "NSData+HexString.h"
#import "NSData+SHA1.h"
@implementation CryptoManager
+ (instancetype)sharedManager {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];


    });
    return sharedMyManager;
}


-(void)generateCertificateWithPublicKey:(NSData *)publicKeyData commonName:(NSString *)cn sign:(NSData *(^)(NSData *hashToSign))signingBlock completition:(void (^)(NSData *certificate))completitionBlock{

    ASN1Composite *asn1=[X509 createX509CertificateWithCommonName:cn publicKey:publicKeyData];

    NSData *unsignedCertificate=[X509 encodeASN1:asn1 useINF:NO];
    NSLog(@"unsigned certificate binary data %@",[unsignedCertificate hexString]);

    NSData *sha1Hash=[unsignedCertificate sha1];

    NSLog(@"hash to sign is %@",[sha1Hash hexString]);
    NSData *signedHash=signingBlock(sha1Hash);
    NSLog(@"signed hash is %@",[signedHash hexString]);

    ASN1Composite *signedCertificateASN1Composite=[X509 signedCertificateWithCertificateData:unsignedCertificate signatureData:signedHash];
    NSData *signedCertificate=[X509 encodeASN1:signedCertificateASN1Composite useINF:NO];


    completitionBlock(signedCertificate);

}
@end
