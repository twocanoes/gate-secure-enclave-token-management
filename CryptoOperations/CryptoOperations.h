//
//  CryptoOperations.h
//  CryptoOperations
//
//  Created by Timothy Perfitt on 5/11/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import <Foundation/Foundation.h>

//! Project version number for CryptoOperations.
FOUNDATION_EXPORT double CryptoOperationsVersionNumber;

//! Project version string for CryptoOperations.
FOUNDATION_EXPORT const unsigned char CryptoOperationsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CryptoOperations/PublicHeader.h>

#import <CryptoOperations/CryptoManager.h>
