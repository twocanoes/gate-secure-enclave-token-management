//
//  ViewController.m
//  TokenContainerApp
//
//  Created by Timothy Perfitt on 5/8/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "ViewController.h"
#import <CryptoTokenKit/CryptoTokenKit.h>
#import "NSData+SHA1.h"
#import "NSData+HexString.h"
#import "Gate-Swift.h"
#define TOKENID @"EBB1F74647B54299A857E298CBEEEAF1"
@interface ViewController()
@property (strong) NSMutableDictionary *tokenCertificates;

@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

-(IBAction)makeAvailableToKeychainCheckboxChanged:(NSButton *)button{


    if (button.state==NSControlStateValueOn){

        [self loadConfig:self];
    }
    else {
        [self unloadConfig:self];
    }
}
-(void)loadConfig:(id)sender{

    NSDictionary *driverConfigDict=[TKTokenDriverConfiguration driverConfigurations];
    TKTokenDriverConfiguration *driverConfig=driverConfigDict[@"com.twocanoes.gate.TokenExtension"];
    NSDictionary *tokenConfigDict=[driverConfig tokenConfigurations];
    TKTokenConfiguration *tokenConfig=tokenConfigDict[TOKENID];

    tokenConfig=[driverConfig addTokenConfigurationForTokenInstanceID:TOKENID];
    NSMutableArray *items=[NSMutableArray array];
    [self updateAvailableCertificatesInto:items];
    tokenConfig.keychainItems=[NSArray arrayWithArray:items];
}

-(void)unloadConfig:(id)sender{


    NSDictionary *driverConfigDict=[TKTokenDriverConfiguration driverConfigurations];
    TKTokenDriverConfiguration *driverConfig=driverConfigDict[@"com.twocanoes.gate.TokenExtension"];


    [driverConfig.tokenConfigurations enumerateKeysAndObjectsUsingBlock:^(TKTokenInstanceID  _Nonnull key, TKTokenConfiguration * _Nonnull obj, BOOL * _Nonnull stop) {
        [driverConfig removeTokenConfigurationForTokenInstanceID:key];

    }];
    NSLog(@"token config unloaded");
}

- (IBAction)makeSEKeys:(id)sender {
    GatedCommunity *gateComm = [[GatedCommunity alloc] init];
    [gateComm genKeyAndCert];
}
- (IBAction)encryptFile:(id)sender {
    GatedCommunity *gateComm = [[GatedCommunity alloc] init];
    [gateComm encryptFile];
}

- (IBAction)decryptFile:(id)sender {
    GatedCommunity *gateComm = [[GatedCommunity alloc] init];
    [gateComm decryptFile];
}

- (IBAction)setPIN:(id)sender {
    GatedCommunity *gateComm = [[GatedCommunity alloc] init];
    [gateComm setPin];
}

-(void)updateAvailableCertificatesInto:(NSMutableArray<TKTokenKeychainItem *> *)items{


    if (!self.tokenCertificates) {
        self.tokenCertificates=[NSMutableDictionary dictionary];
    }
    [self.tokenCertificates removeAllObjects];

    NSData *storedCert = [ NSUserDefaults.standardUserDefaults dataForKey:@"CertificateData"];
    
    if (storedCert && storedCert.length>0) {
        NSLog(@"Populating certificate item with data");

        // Create certificate item.
        SecCertificateRef certificate = SecCertificateCreateWithData(kCFAllocatorDefault, (CFDataRef)storedCert);
        if (certificate == NULL) {
            return;
        }
        NSData *certificateSHA1Hash=[storedCert sha1];
        NSLog(@"certificate sha1 hash: %@",[certificateSHA1Hash hexString]);

        TKTokenObjectID certificateID = [ NSUserDefaults.standardUserDefaults dataForKey:@"KeyRep"];

        int certIDValue2=0x5fc10f;
        TKTokenObjectID certificateID2 = [TKBERTLVRecord dataForTag:certIDValue2];

        TKTokenKeychainCertificate *certificateItem = [[TKTokenKeychainCertificate alloc] initWithCertificate:certificate objectID:certificateID2];
        if (certificateItem == nil) {
            CFRelease(certificate);
            return;
        }
        CFStringRef cn;
        if (SecCertificateCopyCommonName(certificate,&cn) !=errSecSuccess) {
            CFRelease(certificate);
            return;

        }

        TKTokenKeychainKey *keyItem = [[TKTokenKeychainKey alloc] initWithCertificate:certificate objectID:certificateID];

        CFRelease(certificate);

        if (![self.tokenCertificates objectForKey:certificateSHA1Hash]) {


            NSLog(@"certificate label: %@",certificateItem.label);


            // Create key item.
            if (keyItem == nil) {
                return ;
            }
            [keyItem setLabel:[NSString stringWithFormat:@"key"]];

            NSString *label=@"certificate";

            certificateItem.label=label;

            NSMutableDictionary<NSNumber *, TKTokenOperationConstraint> *constraints = [NSMutableDictionary dictionary];
            NSString *pinHash = [NSUserDefaults.standardUserDefaults stringForKey:@"PINHash"];
            if (!pinHash) {
                pinHash = @"NONE";
            }

            constraints[@(TKTokenOperationReadData)] = pinHash;
            constraints[@(TKTokenOperationSignData)] = pinHash;
            constraints[@(TKTokenOperationDecryptData)] = pinHash;
            constraints[@(TKTokenOperationPerformKeyExchange)] = pinHash;

            keyItem.canSign = YES;
            keyItem.suitableForLogin=YES;
            keyItem.canDecrypt=YES;
            keyItem.canPerformKeyExchange = YES;
            keyItem.constraints = constraints;
            [items addObject:certificateItem];
            [items addObject:keyItem];
            [self.tokenCertificates setObject:storedCert forKey:certificateSHA1Hash];

        }
        
    }

//    else {

//
//        NSString *certArrayPath=[[NSBundle mainBundle] pathForResource:@"certs" ofType:@"plist"];
//
//        NSArray *certArray=[NSArray arrayWithContentsOfFile:certArrayPath];
//
//
//        [certArray enumerateObjectsUsingBlock:^(NSDictionary *certInfo, NSUInteger idx, BOOL * _Nonnull stop) {
//            if (!certInfo) {
//                NSLog(@"blank certificate info. skipping");
//                return;
//            }
//            NSString *certDataString=[certInfo objectForKey:@"certificate"];
//            if (!certDataString) {
//                NSLog(@"blank certificate data. skipping");
//                return;
//            }
//            NSData *certData = [[NSData alloc]
//                                initWithBase64EncodedString:certDataString options:NSDataBase64DecodingIgnoreUnknownCharacters];
//
//            if (certData){
//
//                // Create certificate item.
//                SecCertificateRef certificate = SecCertificateCreateWithData(kCFAllocatorDefault, (CFDataRef)certData);
//                if (certificate == NULL) {
//                    return;
//                }
//                NSData *certificateSHA1Hash=[certData sha1];
//                NSLog(@"certificate sha1 hash: %@",[certificateSHA1Hash hexString]);
//
//
//                int certIDValue=0x5fc10b;
//                TKTokenObjectID certificateID = [TKBERTLVRecord dataForTag:certIDValue+idx];
//
//                TKTokenKeychainCertificate *certificateItem = [[TKTokenKeychainCertificate alloc] initWithCertificate:certificate objectID:certificateID];
//                if (certificateItem == nil) {
//                    CFRelease(certificate);
//                    return;
//                }
//                CFStringRef cn;
//                if (SecCertificateCopyCommonName(certificate,&cn) !=errSecSuccess) {
//                    CFRelease(certificate);
//                    return;
//
//                }
//                int certIDValue2=0x5fc10f;
//                TKTokenObjectID certificateID2 = [TKBERTLVRecord dataForTag:certIDValue2+idx];
//
//                TKTokenKeychainKey *keyItem = [[TKTokenKeychainKey alloc] initWithCertificate:certificate objectID:certificateID2];
//
//                CFRelease(certificate);
//
//                if (![self.tokenCertificates objectForKey:certificateSHA1Hash]) {
//                    // Create key item.
//                    if (keyItem == nil) {
//                        return ;
//                    }
//                    [keyItem setLabel:[NSString stringWithFormat:@"key-%li",idx]];
//
//                    NSString *label=[certInfo objectForKey:@"label"];
//
//                    certificateItem.label=label;
//
//                    NSMutableDictionary<NSNumber *, TKTokenOperationConstraint> *constraints = [NSMutableDictionary dictionary];
//                    keyItem.canSign = YES;
//                    keyItem.suitableForLogin=YES;
//                    keyItem.canDecrypt=YES;
//                    keyItem.canPerformKeyExchange = YES;
//
//                    keyItem.constraints = constraints;
//                    [items addObject:certificateItem];
//                    [items addObject:keyItem];
//                    [self.tokenCertificates setObject:certData forKey:certificateSHA1Hash];
//
//                }
//
//            }
//        }];
//
//    }

}
- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
