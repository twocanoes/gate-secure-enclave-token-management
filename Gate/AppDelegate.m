//
//  AppDelegate.m
//  TokenContainerApp
//
//  Created by Timothy Perfitt on 5/8/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "AppDelegate.h"
#import <CryptoOperations/CryptoOperations.h>
#import "Gate-Swift.h"

#import "NSData+HexString.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSData *storedCert = [ NSUserDefaults.standardUserDefaults dataForKey:@"CertificateData"];


    if (!storedCert) {
        GatedCommunity *gateComm = [[GatedCommunity alloc] init];
        [gateComm genKeyAndCert];

    }
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename{
    GatedCommunity *gateComm = [[GatedCommunity alloc] init];

    NSData *data=[NSData dataWithContentsOfFile:filename];
    if (data) {
        [gateComm decryptDataWithFileData:data];
    }
    else NSBeep();

    return YES;

}
@end
