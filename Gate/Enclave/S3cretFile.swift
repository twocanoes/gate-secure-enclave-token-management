//
//  S3cretFile.swift
//  TokenContainerApp
//
//  Created by Joel Rennich on 5/13/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

import Foundation
import CryptoKit

struct S3cretFile: Codable {
    let ephemeralPublicKey: Data
    let salt: Data
    let fileName: Data
    let payload: Data
    var fileNameDecrypted: Data?
    
    enum CodingKeys: String, CodingKey {
        case ephemeralPublicKey = "epk"
        case salt = "s"
        case fileName = "fn"
        case payload = "p"
        case fileNameDecrypted = "fnd"
    }
    
    init?(data: Data, fileNameData: Data) {
        let newkey = P256.KeyAgreement.PrivateKey.init()
        let symKeySalt = SymmetricKey(size: .bits256)
        let salt = symKeySalt.withUnsafeBytes { Data($0) }
        guard let keyRep = UserDefaults.init().data(forKey: "KeyRep") else { return nil }
        guard let privateKey = try? SecureEnclave.P256.KeyAgreement.PrivateKey.init(dataRepresentation: keyRep) else { return nil}
        guard let sharedSecret = try? privateKey.sharedSecretFromKeyAgreement(with: newkey.publicKey) else { return nil }
        let symmetricKey = sharedSecret.x963DerivedSymmetricKey(using: SHA256.self, sharedInfo: salt, outputByteCount: 32)
        guard let sealedMessage = try? ChaChaPoly.seal(data, using: symmetricKey) else { return nil}
        guard let sealedName = try? ChaChaPoly.seal(fileNameData, using: symmetricKey) else { return nil}

        self.ephemeralPublicKey = newkey.publicKey.x963Representation
        self.salt = salt
        self.fileName = sealedName.combined
        self.payload = sealedMessage.combined
        self.fileNameDecrypted = nil
    }
    
    func JSONData() -> Data? {
        let encoder = JSONEncoder.init()
        return try? encoder.encode(self)
    }
    
    mutating func decrypt() -> Data? {
        guard let newKey = try? P256.KeyAgreement.PublicKey.init(x963Representation: ephemeralPublicKey) else {return nil}
        guard let keyRep = UserDefaults.init().data(forKey: "KeyRep") else { return nil }
        guard let privateKey = try? SecureEnclave.P256.KeyAgreement.PrivateKey.init(dataRepresentation: keyRep) else { return nil}
        guard let sharedSecret = try? privateKey.sharedSecretFromKeyAgreement(with: newKey) else { return nil }
        let symmetricKey = sharedSecret.x963DerivedSymmetricKey(using: SHA256.self, sharedInfo: salt, outputByteCount: 32)
        guard let nameSealedBox = try? ChaChaPoly.SealedBox.init(combined: fileName) else {return nil}
        self.fileNameDecrypted = try? ChaChaPoly.open(nameSealedBox, using: symmetricKey)
        guard let sealedBox = try? ChaChaPoly.SealedBox.init(combined: payload) else {return nil}
        return try? ChaChaPoly.open(sealedBox, using: symmetricKey)
    }
}
